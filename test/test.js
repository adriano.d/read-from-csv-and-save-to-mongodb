const mongoose = require('mongoose')
const readCSV = require('../readCSV')
const { uploadToDB, readAllRecordsFromDB } = require('../dbController')
const assert = require('chai').assert

// asynchronously reads in records from CSV, uploads them to a local database, and prints out a single record
const readUploadAndCountRecords = async (recordsToRead, csvPath) => {
    const cheeseRecords = await readCSV(recordsToRead, csvPath)
    await uploadToDB(cheeseRecords)
    return await readAllRecordsFromDB()
}

// Mocha test using Chai assertion library to test that number of CSV records read in matches number saved in DB
describe('CheeseModelRecords',  () => {
    it('should prove that number of records to be read in is equal to number of records stored in mongodb. \n\n(BY ADRIANO DRAMISINO)', () => {
        mongoose.connect('mongodb://localhost/test_ex4', {useUnifiedTopology: true, useNewUrlParser: true}, async () =>  {
            const count = await readUploadAndCountRecords(5, './canadianCheeseDirectory.csv')
            assert.equal(count, 5)
            mongoose.disconnect()
        })
    })
})

