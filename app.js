const mongoose = require('mongoose')
const readCSV = require('./readCSV')
const { uploadToDB, readSingleRecordFromDB } = require('./dbController')

// asynchronously reads in records from CSV, uploads them to a local database, and prints out a single record
const readUploadAndRetrieveOne = async (recordsToRead, csvPath) => {
    const cheeseRecords = await readCSV(recordsToRead, csvPath)
    await uploadToDB(cheeseRecords)
    const singleRecord = await readSingleRecordFromDB()
    console.log(singleRecord)
    mongoose.disconnect()
}

// default db connection, the above function is called once the connection is made
mongoose.connect('mongodb://localhost/ex4', {useUnifiedTopology: true, useNewUrlParser: true}, ()=> {
    readUploadAndRetrieveOne(5, './canadianCheeseDirectory.csv')
})
     







