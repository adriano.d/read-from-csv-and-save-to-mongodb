const mongoose = require('mongoose')

const CheeseSchema = new mongoose.Schema(
    {
        CheeseId: {type: String},
        CheeseName: {type: String},
        Manufacturer: {type: String},
        ManufacturerProvCode: {type: String},
        ManufacturingType: {type: String},
        Website: {type: String},
        FatContentPercent: {type: String},
        MoisturePercent: {type: String},
        Particularities: {type: String},
        Flavour: {type: String},
        Characteristics: {type: String},
        Ripening: {type: String},
        Organic: {type: String},
        CategoryType: {type: String},
        MilkType: {type: String},
        MilkTreatmentType: {type: String},
        RindType: {type: String},
        LastUpdateDate: {type: String},
     }
)

module.exports = mongoose.model('Cheese', CheeseSchema)