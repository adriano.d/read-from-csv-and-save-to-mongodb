const fs = require('fs')
const csv = require('csv-parser')
const Cheese = require('./model')

const readCSV = (numberOfRecordsToRead, filePath) => new Promise((resolve, reject) => {
     const cheeseRecords = []

     // creating read stream to specified file path
     fs.createReadStream(filePath)
     .pipe(csv({headers: false, skipLines: 1}))
     
     // handle error of csv not found by rejecting promise and exiting process
     .on('error', err => {
         reject(err)
         process.exit(1)
     })
 
     // read in lines from csv when data is available
     .on('data', line => {    
         
        // only read in the number of records specified by user
        if(cheeseRecords.length == numberOfRecordsToRead) return
         
        // destructuring object returned from line to choose only the columns we want
        const {0:a,1:b,3:c,5:d,6:e,8:f,10:g,11:h,12:i,14:j,16:k,18:l,20:m,21:n,23:o,25:p,27:q,29:r} = line;
        
        // create new model for every CSV record read in
        const cheeseRecord = new Cheese({
            CheeseId: a,
            CheeseName: b,
            Manufacturer: c,
            ManufacturerProvCode: d,
            ManufacturingType: e,
            Website: f,
            FatContentPercent: g,
            MoisturePercent: h,
            Particularities: i,
            Flavour: j,
            Characteristics: k,
            Ripening: l,
            Organic: m,
            CategoryType: n,
            MilkType: o,
            MilkTreatmentType: p,
            RindType: q,
            LastUpdateDate: r, 
        })

         // push newly created model object to array of model objects
         cheeseRecords.push(cheeseRecord)
    })
 
     // handle end of csv file resolves cheesRecords array
     .on('end', () => resolve(cheeseRecords))
})

module.exports = readCSV
