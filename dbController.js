const mongoose = require('mongoose')
const Cheese = require('./model')

// uploads array of mongoose Schema object to local db and returns a Promise
const uploadToDB = array => new Promise((resolve, reject) => {
    array.forEach((record, index) => {
        record.save()
              .then(() => {
                if (index == array.length-1) { 
                    resolve()
                }
             }, err =>  {
                reject()
                console.error(err)
                mongoose.disconnect()
             })
    })
})

// reads the first record in the 'cheeses' collection, returns a promise
const readSingleRecordFromDB = () => new Promise((resolve, reject) => {
    Cheese.findOne({}, (err, record) => {
        if(err) {
            console.error(err)
            reject()
        }
        resolve(record)
    })
})

// test function which retrieves all records from 'cheeses' collection
const readAllRecordsFromDB = () => new Promise((resolve, reject) => {
    Cheese.countDocuments({}, (err, records) => {
        if(err) {
            console.error(err)
            reject()
        }
        resolve(records)
    })
})

module.exports.uploadToDB = uploadToDB
module.exports.readSingleRecordFromDB = readSingleRecordFromDB
module.exports.readAllRecordsFromDB = readAllRecordsFromDB